package com.example.marketapp.marketapp.repository;

//import az.ingress.marketapp.model.Manager;
import com.example.marketapp.marketapp.model.Manager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManagerRepository extends JpaRepository<Manager, Long> {
}
