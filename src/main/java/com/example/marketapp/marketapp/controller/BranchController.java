package com.example.marketapp.marketapp.controller;

//import az.ingress.marketapp.dto.CreateBranchDto;
//import az.ingress.marketapp.model.Branch;
//import az.ingress.marketapp.repository.genericsearch.SearchCriteria;
//import az.ingress.marketapp.service.BranchService;
import com.example.marketapp.marketapp.dto.BranchDto;
import com.example.marketapp.marketapp.dto.CreateBranchDto;
import com.example.marketapp.marketapp.model.Branch;
import com.example.marketapp.marketapp.repository.genericsearch.SearchCriteria;
import com.example.marketapp.marketapp.service.BranchService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market/{marketId}/branch")
@Valid
public class BranchController {

    private final BranchService branchService;

    @PostMapping
    public void create(@PathVariable Long marketId,
                       @RequestBody CreateBranchDto branchDto) {
        branchService.create(marketId, branchDto);
    }

    @PostMapping("/search")
    public Page<Branch> search(@PathVariable Long marketId,
                               @RequestBody List<SearchCriteria> searchCriteria,
                               Pageable pageable) {
        return branchService.searchByName(marketId, searchCriteria, pageable);
    }
    @GetMapping("/{id}")
    public BranchDto findById(@PathVariable Long id){
        return branchService.findById(id);
    }
    @PostMapping("/{id}")
    public void update(@PathVariable Long id , @RequestBody CreateBranchDto branchDto){
        branchService.update(id,branchDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        branchService.delete(id);
    }
}
