package com.example.marketapp.marketapp.controller;

//import az.ingress.marketapp.dto.CreateManagerDto;
//import az.ingress.marketapp.dto.CreateMarketDto;
//import az.ingress.marketapp.service.ManagerService;
import com.example.marketapp.marketapp.dto.CreateManagerDto;
import com.example.marketapp.marketapp.dto.ManagerDto;
import com.example.marketapp.marketapp.model.Manager;
import com.example.marketapp.marketapp.service.ManagerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market/{marketId}/branch/{branchId}/manager")
@Valid
public class ManagerController {

    private final ManagerService managerService;

    @PostMapping
    public void create(@PathVariable Long marketId,
                       @PathVariable Long branchId,
                       @RequestBody CreateManagerDto managerDto) {
        managerService.create(marketId, branchId, managerDto);
    }

   @GetMapping("/{id}")
    public ManagerDto findById(@PathVariable Long id){
     return managerService.findById(id);
   }
   @PostMapping("/{id}")
    public void update(@PathVariable Long id,
                       @RequestBody CreateManagerDto managerDto){
        managerService.update(id,managerDto);
   }
   @PostMapping("/{id}")
    public void delete(@PathVariable Long id){
        managerService.delete(id);
   }
}
