package com.example.marketapp.marketapp.mapper;


//import az.ingress.marketapp.dto.CreateBranchDto;
//import az.ingress.marketapp.dto.CreateMarketDto;
//import az.ingress.marketapp.model.Branch;
//import az.ingress.marketapp.model.Market;
import com.example.marketapp.marketapp.dto.BranchDto;
import com.example.marketapp.marketapp.dto.CreateBranchDto;
import com.example.marketapp.marketapp.model.Branch;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface BranchMapper {

    Branch dtoToBranch(CreateBranchDto dto);

    BranchDto branchToDto(Branch branch);
    List<BranchDto> dtoToBranchList(List<Branch> branchList);
}
