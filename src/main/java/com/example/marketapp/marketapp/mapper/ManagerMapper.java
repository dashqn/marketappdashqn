package com.example.marketapp.marketapp.mapper;


//import az.ingress.marketapp.dto.CreateManagerDto;
//import az.ingress.marketapp.dto.CreateMarketDto;
//import az.ingress.marketapp.model.Manager;
//import az.ingress.marketapp.model.Market;
import com.example.marketapp.marketapp.dto.CreateManagerDto;
import com.example.marketapp.marketapp.dto.ManagerDto;
import com.example.marketapp.marketapp.model.Manager;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface ManagerMapper {

    Manager managerToDto(CreateManagerDto dto);
    ManagerDto dtoToManager(Manager manager);
    List<Manager> toManagerDtoList(List<Manager> managerList);
}
