package com.example.marketapp.marketapp.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class BranchDto extends CreateBranchDto {
    Long id;
}
