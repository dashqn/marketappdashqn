package com.example.marketapp.marketapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateManagerDto {
    String name;
    String surname;
    Integer age;
    List<CreatePhoneDto> phones;
}
