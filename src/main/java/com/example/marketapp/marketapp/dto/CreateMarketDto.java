package com.example.marketapp.marketapp.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateMarketDto {
    @NotBlank
    @Size(max = 30)
    String name;
    @NotBlank
    String address;
}
