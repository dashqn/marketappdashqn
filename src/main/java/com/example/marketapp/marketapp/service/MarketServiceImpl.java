package com.example.marketapp.marketapp.service;

//import az.ingress.marketapp.dto.CreateMarketDto;
//import az.ingress.marketapp.dto.MarketDto;
//import az.ingress.marketapp.mapper.MarketMapper;
//import az.ingress.marketapp.model.Market;
//import az.ingress.marketapp.repository.MarketRepository;
//import az.ingress.marketapp.repository.genericsearch.CustomSpecification;
//import az.ingress.marketapp.repository.genericsearch.SearchCriteria;
import com.example.marketapp.marketapp.dto.CreateMarketDto;
import com.example.marketapp.marketapp.dto.MarketDto;
import com.example.marketapp.marketapp.mapper.ManagerMapper;
import com.example.marketapp.marketapp.mapper.MarketMapper;
import com.example.marketapp.marketapp.model.Market;
import com.example.marketapp.marketapp.repository.MarketRepository;
import com.example.marketapp.marketapp.repository.genericsearch.CustomSpecification;
import com.example.marketapp.marketapp.repository.genericsearch.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {
    private final MarketRepository marketRepository;
    private final MarketMapper marketMapper;

    @Override
    public void create(CreateMarketDto dto) {
        Optional<Market> byName = marketRepository.findByName(dto.getName());
        if (byName.isPresent()) {
            throw new RuntimeException();
        }
        Market market = marketMapper.dtoToMarket(dto);
        marketRepository.save(market);
    }

    @Override
    public List<Market> findAll() {
        return marketRepository.findAll();
    }

    @Override
    public MarketDto findById(Long id) {
        Market market = marketRepository.findById(id).orElseThrow(RuntimeException::new);
        return marketMapper.marketToDto(market);
    }

    @Override
    public void update(Long id, CreateMarketDto marketDto) {
        marketRepository.findById(id).orElseThrow(RuntimeException::new);
        Market market = marketMapper.dtoToMarket(marketDto);
        market.setId(id);
        marketRepository.save(market);
        marketMapper.marketToDto(market);
    }

    @Override
    public void delete(Long id) {
        marketRepository.findById(id).orElseThrow(RuntimeException::new);
        marketRepository.deleteById(id);
        delete(id);
    }

    @Override
    public Collection<Market> searchByName(List<SearchCriteria> searchCriteria) {
        CustomSpecification<Market> specification = new CustomSpecification<>(searchCriteria);
    return marketRepository.findAll(specification);
    }
}
