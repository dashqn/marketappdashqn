package com.example.marketapp.marketapp.service;

//import az.ingress.marketapp.dto.CreateManagerDto;
//import az.ingress.marketapp.mapper.ManagerMapper;
//import az.ingress.marketapp.model.Manager;
//import az.ingress.marketapp.repository.BranchRepository;
import com.example.marketapp.marketapp.dto.CreateManagerDto;
import com.example.marketapp.marketapp.dto.ManagerDto;
import com.example.marketapp.marketapp.mapper.ManagerMapper;
import com.example.marketapp.marketapp.model.Manager;
import com.example.marketapp.marketapp.repository.BranchRepository;
import com.example.marketapp.marketapp.repository.ManagerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ManagerServiceImpl implements ManagerService {
    private final BranchRepository branchRepository;
    private final ManagerRepository managerRepository;
    private final ManagerMapper managerMapper;

    @Override
    public void create(Long marketId, Long branchId, CreateManagerDto managerDto) {
        var branch = branchRepository.findById(branchId).orElseThrow(RuntimeException::new);
        if (!branch.getMarket().getId().equals(marketId)) {
            throw new RuntimeException();
        }
        Manager manager = managerMapper.managerToDto(managerDto);
        branch.setManager(manager);
        branchRepository.save(branch);
    }

    @Override
    public ManagerDto findById(Long id) {
        Manager managerDb = managerRepository.findById(id).orElseThrow(RuntimeException::new);
        return managerMapper.dtoToManager(managerDb);
    }

    @Override
    public List<Manager> findAll() {
        return managerRepository.findAll();
    }

    @Override
    public void update(Long id, CreateManagerDto managerDto) {
        Manager manager = managerRepository.findById(id).orElseThrow(RuntimeException::new);
        managerMapper.managerToDto(managerDto);
        manager.setId(id);
        managerRepository.save(manager);
        managerMapper.dtoToManager(manager);
    }

    @Override
    public void delete(Long id) {
        managerRepository.findById(id).orElseThrow(RuntimeException::new);
        managerRepository.deleteById(id);
        delete(id);

    }
}
