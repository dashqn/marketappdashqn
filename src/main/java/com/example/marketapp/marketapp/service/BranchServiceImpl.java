package com.example.marketapp.marketapp.service;

//import az.ingress.marketapp.dto.CreateBranchDto;
//import az.ingress.marketapp.mapper.BranchMapper;
//import az.ingress.marketapp.model.Branch;
//import az.ingress.marketapp.model.Market;
//import az.ingress.marketapp.repository.BranchRepository;
//import az.ingress.marketapp.repository.MarketRepository;
//import az.ingress.marketapp.repository.genericsearch.CustomSpecification;
//import az.ingress.marketapp.repository.genericsearch.SearchCriteria;
import com.example.marketapp.marketapp.dto.BranchDto;
import com.example.marketapp.marketapp.dto.CreateBranchDto;
import com.example.marketapp.marketapp.mapper.BranchMapper;
import com.example.marketapp.marketapp.model.Branch;
import com.example.marketapp.marketapp.model.Manager;
import com.example.marketapp.marketapp.model.Market;
import com.example.marketapp.marketapp.repository.BranchRepository;
import com.example.marketapp.marketapp.repository.ManagerRepository;
import com.example.marketapp.marketapp.repository.MarketRepository;
import com.example.marketapp.marketapp.repository.genericsearch.CustomSpecification;
import com.example.marketapp.marketapp.repository.genericsearch.SearchCriteria;
import jakarta.persistence.criteria.*;
import lombok.RequiredArgsConstructor;
import org.hibernate.engine.spi.Managed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final ManagerRepository managerRepository;
    private final BranchMapper branchMapper;




    @Override
    public Page<Branch> searchByName(Long marketId, List<SearchCriteria> searchCriteria, Pageable pageable) {
        List<SearchCriteria> newCriteria = searchCriteria.stream()
                .filter(criteria -> !criteria.getKey().equals("name")).toList();
        CustomSpecification<Branch> customSpecification = new CustomSpecification<>(newCriteria);

        Specification<Branch> marketIdSpec = (root, query, criteriaBuilder) -> {
            Join<Branch, Market> marketBranches = root.join("market");
            return criteriaBuilder.equal(marketBranches.get("id"), marketId);
        };
        return branchRepository.findAll(customSpecification.and(marketIdSpec), pageable);
    }

    @Override
    public void create(Long marketId , CreateBranchDto branchDto) {
        Market market = marketRepository.findById(marketId).orElseThrow(RuntimeException::new);
        Branch branch = branchMapper.dtoToBranch(branchDto);
        branch.setMarket(market);
        branchRepository.save(branch);

    }


    @Override
    public BranchDto findById(Long id) {
        Branch branch = branchRepository.findById(id).orElseThrow(RuntimeException::new);
        return branchMapper.branchToDto(branch);
    }

    @Override
    public void update(Long id, CreateBranchDto branchDto) {
        Branch  branch = branchRepository.findById(id).orElseThrow(RuntimeException::new);
        branchMapper.dtoToBranch(branchDto);
        branch.setId(id);
        branchRepository.save(branch);
        branchMapper.branchToDto(branch);
    }

    @Override
    public void updateMarket(Long branchId, Long marketId) {
        Branch branch = branchRepository.findById(branchId).orElseThrow(RuntimeException::new);
        Market market = marketRepository.findById(marketId).orElseThrow(RuntimeException::new);
        branch.setMarket(market);
        branchRepository.save(branch);
    }

    @Override
    public void updateManager(Long branchId, Long managerId) {
        Branch branch = branchRepository.findById(branchId).orElseThrow(RuntimeException::new);
        Manager manager = managerRepository.findById(managerId).orElseThrow(RuntimeException::new);
        branch.setManager(manager);
        branchRepository.save(branch);
    }

    @Override
    public void delete(Long id) {
        branchRepository.findById(id).orElseThrow(RuntimeException::new);
        branchRepository.deleteById(id);
        delete(id);
    }
}
