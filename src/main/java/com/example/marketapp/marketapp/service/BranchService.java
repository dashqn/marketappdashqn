package com.example.marketapp.marketapp.service;

//import az.ingress.marketapp.dto.CreateBranchDto;
//import az.ingress.marketapp.model.Branch;
//import az.ingress.marketapp.repository.genericsearch.SearchCriteria;
import com.example.marketapp.marketapp.dto.BranchDto;
import com.example.marketapp.marketapp.dto.CreateBranchDto;
import com.example.marketapp.marketapp.dto.CreateMarketDto;
import com.example.marketapp.marketapp.dto.MarketDto;
import com.example.marketapp.marketapp.model.Branch;
import com.example.marketapp.marketapp.model.Market;
import com.example.marketapp.marketapp.repository.genericsearch.SearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

public interface BranchService {

    void create(Long marketId, CreateBranchDto branchDto);

    BranchDto findById(Long id);


    void update(Long id, CreateBranchDto branchDto);

    void updateMarket(Long branchId,Long marketId );
    void updateManager(Long branchId,Long managerId);
    void delete(Long id);

    Page<Branch> searchByName(Long marketId, List<SearchCriteria> searchCriteria, Pageable pageable);
}
