package com.example.marketapp.marketapp.service;

//import az.ingress.marketapp.dto.CreateManagerDto;

import com.example.marketapp.marketapp.dto.CreateManagerDto;
import com.example.marketapp.marketapp.dto.ManagerDto;
import com.example.marketapp.marketapp.model.Manager;

import java.util.List;

public interface ManagerService {
    void create(Long marketId, Long branchId, CreateManagerDto managerDto);

    ManagerDto findById(Long id);

    List<Manager> findAll();

    void update(Long id,CreateManagerDto managerDto);
    void delete(Long id);


}
